#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define MAXY 170//100 // lenth of max screen size
#define MAXX 170//100


typedef struct point {
    unsigned x;
    unsigned y;
} POINT;// end of POINT structure declare

int convexHull(POINT *, POINT *, int);
#endif