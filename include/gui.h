#ifndef _GUI_H_
#define _GUI_H_

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<gtk/gtk.h>
#include<cairo.h>

gboolean on_expose_event(GtkWidget *, GdkEventExpose *, gpointer);
void value_changed(GtkSpinButton *, gpointer);
void toggled_callback(GtkToggleButton *, gpointer);

#endif