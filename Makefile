CC = clang
INC = -I ./include
OBJ = src/*.c

convexhull: ${OBJ}
	$(CC) -o $@ ${OBJ} ${INC} `pkg-config --libs --cflags gtk+-2.0`