#include "gui.h"
#include "algorithm.h"

int arrsize = 1000;

static POINT arr[10000];
static POINT index[10000];

int n, temp = 0;

gboolean on_expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    cairo_t *cr = gdk_cairo_create (widget->window);
    int i = 0;

    if(arrsize <= 10) arrsize = 10;
    GdkRectangle da;            /* GtkDrawingArea size */
    gint unused = 0;

    /* Determine GtkDrawingArea dimensions */
    gdk_window_get_geometry(widget->window, &da.x, &da.y, &da.width, &da.height, &unused);

    /* Draw on a black background */
    cairo_set_source_rgb (cr, 0, 0, 0);
    cairo_paint (cr);

    /* Change the transformation matrix */
    cairo_translate (cr, 10.0, 10.0);
    cairo_scale (cr, 4.0, 4.0);  
    /*main algorithm*/
    if(temp >= n){
        n = convexHull(arr, index, arrsize);
    }
    
    cairo_set_line_cap  (cr, CAIRO_LINE_CAP_ROUND);
    cairo_set_line_width (cr, 0.5);
    /*draw x*/
    cairo_move_to (cr, 0.0, MAXY);
    cairo_line_to (cr, MAXX, MAXY);
    /*draw y*/
    cairo_move_to (cr, 0.0, MAXY);
    cairo_line_to (cr, 0.0, 0.0);
    cairo_set_source_rgba (cr, 1.0, 1.0, 1.0, 1.0);
    cairo_stroke (cr);
    /**/
    cairo_set_line_width (cr, 0.5);
    cairo_set_source_rgba (cr, 1, 1, 0.2, 1.0);
    cairo_move_to (cr, index[0].x, MAXY - index[0].y);
    if(temp <= n){
        for(i = 0; i <= temp; i++){
            cairo_line_to (cr, index[i].x, MAXY - index[i].y);
            //printf("index[%d] = %u, %u\n", i, index[i].x, index[i].y);
        }
        temp++;
        if(temp ==n) {
            temp = 0;
            n = 0;
        }
    } 
    cairo_stroke (cr);


    cairo_set_source_rgba (cr, 1, 0.2, 0.2, 1.0);
    /*draw point and line*/
    cairo_set_line_width (cr, 1.0);
    for(i = 0; i < arrsize; i++){
        cairo_move_to (cr, arr[i].x - 0.00001, MAXY - arr[i].y - 0.00001);
        cairo_line_to (cr, arr[i].x, MAXY - arr[i].y);
    }
    cairo_stroke (cr);
    
    //cairo_stroke (cr);

    cairo_destroy (cr);
    return FALSE;
}

void value_changed(GtkSpinButton *spinButton, gpointer data) 
{
    gint value = gtk_spin_button_get_value_as_int(spinButton);
    arrsize = value;
}

void toggled_callback(GtkToggleButton *toggleBtn, gpointer data) 
{
    gtk_widget_queue_draw( GTK_WIDGET(data) );
}
