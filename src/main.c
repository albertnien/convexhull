//use `pkg-config --libs --cflags gtk+-2.0`
#include "gui.h"

#define WIDTH   720//420
#define HEIGHT  750//470

int main (int argc, char **argv)
{
    GtkWidget *window;
    GtkWidget *plot;
    GtkWidget *table;
    GtkWidget *vPaned;
    GtkWidget *spinButton;
    GtkWidget *button;
    //GtkWidget *label;

    gtk_init (&argc, &argv);

    // create new window
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_size_request(window, WIDTH, HEIGHT);
    gtk_window_set_title (GTK_WINDOW (window), "Convex Hull");
    gtk_window_set_resizable(GTK_WINDOW(window), TRUE);
    g_signal_connect (G_OBJECT (window), "destroy", gtk_main_quit, NULL);

    vPaned = gtk_vpaned_new();
    
    //texinput && RUN BOTTUN
    spinButton = gtk_spin_button_new_with_range(10.0, 100.0, 1.0);
    button = gtk_toggle_button_new_with_label("RUN");
    //label = gtk_label_new(NULL);
    table = gtk_table_new(2, 2, TRUE);

    gtk_table_attach_defaults(GTK_TABLE(table), gtk_label_new("Size"), 0, 1, 0, 1);   
    gtk_table_attach_defaults(GTK_TABLE(table), spinButton, 1, 2, 0, 1);
    //gtk_table_attach_defaults(GTK_TABLE(table), gtk_label_new("point"), 0, 1, 0, 1);
    gtk_table_attach_defaults(GTK_TABLE(table), button, 1, 2, 1, 2);

    //plot data area
    plot = gtk_drawing_area_new ();
    gtk_paned_pack2(GTK_PANED(vPaned), table, FALSE, FALSE);
    gtk_paned_pack1(GTK_PANED(vPaned), plot, FALSE, FALSE);
    gtk_paned_set_position(GTK_PANED(vPaned), HEIGHT - 50);
    
    gtk_container_add(GTK_CONTAINER(window), vPaned);
    
    g_signal_connect(GTK_OBJECT(spinButton), "value_changed", G_CALLBACK(value_changed), NULL);
    g_signal_connect(G_OBJECT (plot), "expose-event", G_CALLBACK (on_expose_event), NULL);
    g_signal_connect(GTK_OBJECT(button), "toggled", G_CALLBACK(toggled_callback), plot);
    
    gtk_widget_show_all (window);
    gtk_main ();

    return 0;
}