#include<stdio.h>
#include<stdlib.h>
#include<time.h>
//#include <gtk/gtk.h>
#include "algorithm.h"

void bubble(POINT *arr, int len)
{
    int i, j;
    POINT temp;
    for(i = len; i > 0; i--) {
        for(j = 0; j < i - 1; j++) {
            if(arr[j].y > arr[j + 1].y) {
                temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    for(i = len; i > 0; i--) {
        for(j = 0; j < i - 1; j++) {
            if(arr[j].x > arr[j + 1].x) {
                temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int arrRand(POINT *arr, int len)
{
    srand(time(NULL));
    for(int i = 0; i < len; i++) {
        arr[i].x = rand() % MAXX;
        arr[i].y = rand() % MAXY;
    }
    return 0;
}// end of arrRand

int cross(POINT *init, POINT *P1, POINT *P2)
{
    /*first : p1 - init =>x1: p1.x - init.x y1: p1.y - init.y
     *second: p2 - p1   =>x2: p2.x - p1.x y2: p2.y - p1.y
     *|x1 y1|
     *|x2 y2|
     */
    return (P1->x - init->x)*(P2->y - init->y) - (P1->y - init->y)*(P2->x - init->x);//smaller than 0 => pop last point 
}

void printarr(POINT *arr, POINT *index, int lenarr, int lenindex)
{
    int i;
    for(i = 0; i < lenarr; i++) {
        printf("arr[%d] = %u, %u\n", i, arr[i].x, arr[i].y);
    }
    for(i = 0; i <= lenindex; i++) {
        printf("index[%d] = %u, %u\n", i, index[i].x, index[i].y);
    }
}

int convexHull(POINT *arr, POINT *index, int len)
{
    int i, j = 0, last = 0;
    
    arrRand(arr, len);
    bubble(arr, len);//n2
    
    for(i = 0; i < len; i++) {
        while(j >= 2 && cross(index + j - 2, index + j - 1, arr + i) > 0) {
            j--;
        }
        index[j] = arr[i];
        j++;
    }
    last = j + 1;
    for(i = len - 2; i >= 0; i--) {
        while(j >= last && cross(index + j - 2, index + j - 1, arr + i) > 0) {
            j--;
        }
        index[j] = arr[i];
        j++;
    }

    printarr(arr, index,len, j - 1);

    if(index[j - 1].x != index[0].x | index[j - 1].y != index[0].y)
        printf("err\n");
    return j;
}




























